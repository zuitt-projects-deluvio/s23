// CREATE

db.users.insertMany([
			{
				"firstName" : "Tony",
				"lastName" : "Stark",
				"email" : "starkindustries@mail.com",
				"password" : "imironman",
				"isAdmin" : false
			},
			{
				
				"firstName" : "Bruce",
				"lastName" : "Banner",
				"email" : "bannerbruce@mail.com",
				"password" : "incrediblehulk",
				"isAdmin" : false
			},
			{
				
				"firstName" : "Natasha",
				"lastName" : "Romanoff ",
				"email" : "romanoffnat@mail.com",
				"password" : "blackwidow",
				"isAdmin" : false
			},
			{
				
				"firstName" : "Steve",
				"lastName" : "Rogers",
				"email" : "rogersteve@mail.com",
				"password" : "capnamerica",
				"isAdmin" : false
			},
			{
				
				"firstName" : "Clint",
				"lastName" : "Barton",
				"email" : "bratonclint@mail.com",
				"password" : "hawkeye",
				"isAdmin" : false
			}

		])



db.courses.insertMany([
			{
				"name" : "Learn Responsive Design",
				"price" : "1000",
				"isActive" : false
			},
			{
				"name" : "PHP",
				"price" : "1500",
				"isActive" : false
			},
			{
				"name" : "MongoDB",
				"price" : "3000",
				"isActive" : false
			}
		])



// READ
db.users.find({"isAdmin" : false})



// UPDATE
db.users.updateOne({}, {$set: {"isAdmin" : true}})

db.courses.updateOne({}, {$set: {"isActive" : true}})


// DELETE

db.courses.deleteMany({"isActive" : false})